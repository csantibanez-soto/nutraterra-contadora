import RPi.GPIO as GPIO
import time

relay1 = 16
relay2 = 17
relay3 = 22
relay4 = 27
relay5 = 21

endstop_max = 25
endstop_min = 26

left_en = 23
left_pwm = 12
right_en = 24
right_pwm = 13

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

GPIO.setup(relay1, GPIO.OUT)
GPIO.setup(relay2, GPIO.OUT)
GPIO.setup(relay3, GPIO.OUT)
GPIO.setup(relay4, GPIO.OUT)
GPIO.setup(relay5, GPIO.OUT)

GPIO.setup(endstop_max, GPIO.IN)
GPIO.setup(endstop_min, GPIO.IN)

GPIO.setup(left_en, GPIO.OUT)
GPIO.setup(left_pwm, GPIO.OUT)
GPIO.setup(right_en, GPIO.OUT)
GPIO.setup(right_pwm, GPIO.OUT)

GPIO.output(left_en, GPIO.HIGH)
GPIO.output(right_en, GPIO.HIGH)

GPIO.output(left_pwm, GPIO.LOW)
GPIO.output(right_pwm, GPIO.LOW)
    
# while True:
#     max_state = GPIO.input(endstop_max)
#     min_state = GPIO.input(endstop_min)
#     print("MAX: ", max_state, "MIN: ", min_state)
#     GPIO.output(relay1, GPIO.HIGH)
#     time.sleep(5)
#     GPIO.output(relay1, GPIO.LOW)
#     time.sleep(5)
#     GPIO.output(relay2, GPIO.HIGH)
#     time.sleep(5)
#     GPIO.output(relay2, GPIO.LOW)
#     time.sleep(5)
#     GPIO.output(relay3, GPIO.HIGH)
#     time.sleep(5)
#     GPIO.output(relay3, GPIO.LOW)
#     time.sleep(5)
#     GPIO.output(relay4, GPIO.HIGH)
#     time.sleep(5)
#     GPIO.output(relay4, GPIO.LOW)
#     time.sleep(5)
#     GPIO.output(relay5, GPIO.HIGH)
#     time.sleep(5)
#     GPIO.output(relay5, GPIO.LOW)
#     time.sleep(5)